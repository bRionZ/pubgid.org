---
title: Donate Us
date: 2018-10-19T10:08:15.000+00:00
decription: ''
type: page
fimage: "/"
description: Donate Us

---
### Donasi Anda Membantu Kami

Jadilah orang bijak dalam memberi, ukur kemampuan Anda sebelum menyisihkannya untuk Kami.

<div class="text-center">
<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
<input type="hidden" name="cmd" value="_s-xclick">
<input type="hidden" name="hosted_button_id" value="TZR9LM2ZYSJYN">
<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
<img alt="paypal donation" border="0" src="https://www.paypalobjects.com/id_ID/i/scr/pixel.gif" width="1" height="1">
</form>
</div>