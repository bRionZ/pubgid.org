+++
date = "2018-07-24T15:06:58+07:00"
description = "Kebijakan Privasi untuk Website PUBGID"
fimage = "/uploads/Desktop Screenshot 2018.07.24 - 15.14.01.31.jpg"
title = "Kebijakan Privasi untuk Website PUBGID"
type = "page"

+++
PUBGID berkomitmen secara ketat terhadap keamanan dan perlindungan privasi bagi pengunjung untuk www.pubgid.org

**Informasi yang dikumpulkan secara otomatis**  
Saat mengakses website PUBGID, file yang dikenal sebagai cookies dikirim ke komputer pengunjung. Ini adalah praktik yang umum di internet. Pada PUBGID, file ini hanya digunakan untuk keperluan statistik (untuk mengukur lalu lintas dan perilaku situs-user). Cookies yang dikirimkan oleh situs PUBGID tidak akan digunakan untuk mengidentifikasi setiap pengunjung pada khususnya, Cookies dapat dihapus kapan saja pengunjung inginkan. Mereka juga dapat menavigasi situs PUBGID dengan opsi cookie dinonaktifkan.

**Informasi yang disediakan oleh pengunjung**  
Pada halaman Kontak, bagian berisi form kontak dimana pengunjung mengirimkan informasi seperti nama dan email jika mereka ingin menghubungi PUBGID, informasi spesifik permintaan, atau berlangganan newsletter. PUBGID hanya menggunakan informasi ini untuk berkomunikasi dengan pengunjung dan tidak berbagi dengan pihak ketiga.

**Keamanan informasi**  
PUBGID berusaha untuk terus meningkatkan standar keamanan di websitenya. Namun, kami tidak bertanggung jawab atas kehilangan data akibat dari insiden di luar kendali.