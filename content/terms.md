+++
date = "2018-07-13T18:28:08+07:00"
description = "Aturan-aturan pada situs Komunitas PUBG Indonesia"
fimage = "/uploads/Desktop Screenshot 2018.07.24 - 15.14.01.31.jpg"
title = "Peraturan"
type = "page"

+++
PERATURAN UMUM KOMUNITAS PUBG INDONESIA :

Komunitas PUBG INDONESIA merupakan sebuah sarana bertemu, berbagi, bermain dan belajar bersama Player PUBG di Indonesia khususnya di komunitas PUBG INDONESIA.

PERATURAN [DISCORD](https://discord.me/pubgid) PUBG INDONESIA :

* Admin dan Moderator akan menggunakan penilaian mereka sendiri ketika beberapa member yang melanggar peraturan.
* Jangan SPAM, TOXIC, BAIT, dan SARA.
* Jangan share CHEAT, PORN, PHISHING, dan SCAM.
* Jangan berdebat yang tidak bermanfaat.
* Jangan share link discord lain dengan metode apapun.
* Member yang melanggar peraturan akan dikenakan sanksi MUTE atau BAN otomatis.
* Member yang dikenakan sanksi MUTE atau BAN akan dilepas apabila sudah mengakui kesalahannya.
* Jangan secara terbuka menuduh pemain lain melakukan pelanggaran IN GAME. Harap selalu menggunakan bantuan dari PUBG untuk melaporkan pemain IN GAME ke [**_https://support.pubg.com/_**](https://support.pubg.com/ "https://support.pubg.com/") [**_https://litesupport.pubg.com/_**](https://litesupport.pubg.com/ "https://litesupport.pubg.com/")
* Semua member wajib mengikuti peraturan diatas, jika melanggar peraturan diatas maka tidak ada alasan untuk bertanya kesalahan, Terima kasih.

10 PERATURAN TAMBAHAN KOMUNITAS PUBG INDONESIA :

 1. Gunakan akal sehat saat posting.
 2. Mohon membawa etika dalam berinternet. untuk referensi [**_https://kask.us/hsopt_**](https://kask.us/hsopt "https://kask.us/hsopt")
 3. Troll/Spam/Bait/Bash/Flame akan mendapat warning dari admin, dan postingan bersangkutan akan dihapus.
 4. Posting yang berbau SARA, PORN, dan SPAM tidak diizinkan.
 5. Dilarang membawa masalah pribadi yang dapat memicu Cyber-Bullying ke dalam Komunitas.
 6. Dilarang bertanya hal yang tidak penting di dalam grup, seperti,"Kenapa PUBG saya tidak bisa?" Mending jelaskan problem nya, atau selalu cek Berita dan Media informasi Official PUBG, jika memang tidak ada masalah maka itu sebuah masalah pribadi (Personal eror). bisa cek di link [**_https://twitter.com/pubg_**](https://twitter.com/pubg "https://twitter.com/pubg")  
    [**_https://www.facebook.com/PUBG/_**](https://www.facebook.com/PUBG/ "https://www.facebook.com/PUBG/")  
    [**_https://store.steampowered.com/news/?appids=578080_**](https://store.steampowered.com/news/?appids=578080&appgroupname=PLAYERUNKNOWN%27S+BATTLEGROUNDS&feed=steam_community_announcements "https://store.steampowered.com/news/?appids=578080&appgroupname=PLAYERUNKNOWN%27S+BATTLEGROUNDS&feed=steam_community_announcements")
 7. Reply komentar dengan foto yang bersifat menghina, mengganggu, akan dihapus.
 8. Untuk pertanyaan tentang performa gaming PC terhadap game tertentu, kunjungi situs [**_http://systemrequirementslab.com/cyri_**](http://systemrequirementslab.com/cyri "http://systemrequirementslab.com/cyri")
 9. Komunitas PUBG INDONESIA TIDAK MENDUKUNG PIRACY/BAJAKAN. Bagi yang bertanya link download, bertanya masalah saat install game bajakan, jual beli bajakan, maupun yang menyediakan link postingan akan dihapus. (diharapkan member jangan ngebash, troll, atau flamming TS yang ngepost. gunakan cara halus seperti "maaf kita tidak bisa bantu masalah game bajakan" atau kata kata lainnya) (Note : bukan berarti yang pakai bajakan ga bisa share SS game ato ngebahas game disini)
10. Out of Topic diperbolehkan, tetapi jangan terlalu melenceng jauh dari pokok pembahasan tentang game.
11. Dimohon untuk tidak mengadakan Giveaway PALSU