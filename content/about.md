---
title: Tentang
date: 2018-07-02T03:49:43.000+00:00
type: page
fimage: "/uploads/abt.jpg"
subheading: 
description: Tentang

---
PUBG Indonesia adalah komunitas para pemain game [Player Unknown's Battlegrounds](https://store.steampowered.com/app/578080/PLAYERUNKNOWNS_BATTLEGROUNDS/) yang berkumpul pada satu wadah untuk berbagi dan bermain bersama. Selain untuk sarana pengikat tali silaturahmi, kami juga mendukung perkembangan skill dan talenta para pemain PUBG Indonesia untuk menjadi lebih baik dan bersaing di kancah Internasional.

Kami memiliki lebih dari 2000 pengguna aktif setiap harinya. Mereka datang untuk berkumpul dan bermain bersama dengan pemain-pemain lain di Indonesia. Kami memiliki sistem lobby yang manual, dimana semua orang bisa masuk ke Jenis permainan yang di inginkan seperti **Squad FPP**, **Squad TPP**, **Duo FPP**, **Duo TPP**, dan **ESport Mode**. 

> Salam Chicken Dinner Gan!

_Event Community & Tournament by_ [_PUBG_](https://www.pubg.com/) _Partners, Situs ini dibuat oleh_ [_Oka bRionZ_](https://www.okabrionz.com)